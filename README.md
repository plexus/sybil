# Plexus Rosetta

## Initial Ideas

- i18n formatted string management in SQLite database
- Each string tagged by unique URN tag and autogenerated integer ID
- REST API commands
- GET: Gets existing string(s)
- POST: Adds new string
- PATCH: Updates existing string
- DELETE: Deletes existing string


### GET

- Gets string details with integer ID 123:
  `rosetta --id=123 get`

- Gets string details with integer ID 123 in JSON format:
  `rosetta --id=123 --json`

- Gets string details for URN urn:my:string and locale en_GB:
  `rosetta --id=urn:my:string --locale=en_GB get` (returns string in en_GB)

- Gets details for all strings with URN urn:my:string:
  `rosetta --id=urn:my:string get`


### POST

- Adds new en_US formatted string with URN urn:my:string;
  returns int ID:
  `rosetta --id=urn:my:string --locale=en_US --string="sample number %d" post`

- Adds new en_US formatted string with URN urn:my:string;
  returns result in JSON:
  `rosetta --id=urn:my:string --json --locale=en_GB --string="sample" post`


### PATCH

- Updates string for a given integer ID:
  `rosetta --id=123 --string="new string %s" patch`

- Updates string for a given integer ID and returns response in JSON:
  `rosetta --id=123 --json --string="new string %s" patch`

- Updates string for a given URN and locale:
  `rosetta --id=urn:my:string --locale=en_GB --string="new string %s" patch`


### DELETE

- Delete string with ID of 123:
 `rosetta --id=123 delete`
 
- Delete string with URN urn:my:string for en_GB locale:
 `rosetta --id=urn:my:string delete`

- Delete all locale strings with URN urn:my:string:
 `rosetta --id=urn:my:string --locale=en_GB delete`

- Delete string with ID of 123 and return response in JSON:
 `rosetta --id=123 --json delete`


## Planned Iterations

- Version 0.1.0: Tooling + binary man page skeletons
- Version 1.0.0: Post + BDD tests + binary man page
- Version 1.1.0: Get + BDD tests + binary man page
- Version 1.2.0: Put + BDD tests + binary man page
- Version 1.3.0: Delete + BDD tests + binary man page
- Version 2.0.0: C implementation
- Version 2.1.0: Library man pages
- Version 2.2.0: Tooling man pages
- Version 2.3.0: Unit tests
- Version 3.0.0: FastCGI web server
