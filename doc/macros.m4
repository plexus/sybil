dnl Override default quoting
changequote(<<, >>) dnl

dnl Content of AUTHORS man page section for binary utilities
define(__authors_utility__,
<<Plexus Rosetta is developed and maintained by
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org Ns .
.Pp
Copyright (C) 2022
.An -nosplit
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org Ns .
SPDX-License-Identifier: BSD-3-Clause. Plexus Rosetta is free and open
source software: you are free to use and redistribute it under the provisions of
the BSD-3-Clause License. There is NO WARRANTY, to the extent permitted by law.
See the
.Sy LICENSE
file provided with the source code for the full license text; alternatively, run
the
.Sy $1
utility with the
.Fl L
flag.>>)


dnl Content of BUGS man page section
define(__bugs__,
<<Please report any bugs to
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org
mentioning all relevant details.>>)


dnl Content of DIAGNOSTICS section of tool script man pages
define(__diagnostics_tool__,
<<The following error codes are reported in case of an error:
.Bl -diag
.It 1
Command line parse error resulting from a malformed invocation of the
.Sy $1
utility. This can be fixed by correctly invoking the
.Sy $1
utility.
.It 2
Any other error encountered during the execution of the
.Sy $1
utility. This is usually unrecoverable, resulting from either a previously
undetected bug in the utility or an error condition in the host OS.
.El
.Pp
In both cases, a failure message is printed to
.Pa stderr
and optionally logged to
.Ev LOGFILE Ns .>>)


dnl Content of EXIT STATUS section of tool script man pages
define(__exit_tool__,
<<.Ex -std $1
See
.Sx DIAGNOSTICS
for more details on the error codes.>>)


dnl Content of the HISTORY man page section
define(__history__,
<<The latest version of the Plexus Rosetta project is available from the public
mirrors at CodeBerg and GitHub.>>)

dnl help options for DESCRIPTION man page section
define(__help_options__,
<<.Bl -tag -width Ds
.It Fl H
Show help on usage; cannot be used with any other flags.
.It Fl L
Show license details; cannot be used with any other flags.
.It Fl V
Show version information; cannot be used with any other flags.
.El>>)

dnl help options with long flags for DESCRIPTION man page section
define(__help_options_long__,
<<.Bl -tag -width Ds
.It Fl H, Fl -help
Show help on usage; cannot be used with any other flags.
.It Fl L, Fl -license
Show license details; cannot be used with any other flags.
.It Fl V, Fl -version
Show version information; cannot be used with any other flags.
.El>>)
