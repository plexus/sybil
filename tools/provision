#!/bin/sh
# _*_ mode: sh; _*_
# SPDX-License-Identifier: BSD-3-Clause
#
# plexus/sybil/tools/provision -- provisioning tool for Plexus Sybil.
# This file is part of Plexus Sybil.
# See pxsyb.prov(8) for documentation.
#
# Copyright (c) 2022 Abhishek Chakravarti.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#
# EXTERNAL SOURCES
#


. /usr/local/src/ru.sh/log.sh
. /usr/local/src/ru.sh/os.sh
. /usr/local/src/ru.sh/su.sh
. /usr/local/src/ru.sh/pkg.sh


#
# GLOBAL VARIABLES
#


# Command line options; provider option will be used when bhyve is supported
# provision -d freebsd -v 13.1 -p vagrant
VERSION=
SSHKEY="$HOME/.ssh/id_rsa"
SCRIPT_VERSION=1
YES=1
HELP=1
LICENSE=1
FORCE=1
EMSG='invalid usage; run ./provision -H to get help'


#
# COMMAND LINE FUNCTIONS
#


# Parses command line
cmd_parse()
{
    ARGC=$#

    while getopts 'HLVv:k:fy' o; do
	case $o in
	    H) HELP=0
	       ;;
	    L) LICENSE=0
	       ;;
	    V) SCRIPT_VERSION=0
	       ;;
	    v) VERSION=$OPTARG
	       ;;
	    k) SSHKEY=$OPTARG
	       ;;
	    f) FORCE=0
	       ;;
	    y) YES=0
	       ;;
	    *) log_fail "$EMSG"
	       ;;
	esac
    done

    for DISTRO; do : ; done
}


# Sanity check for command line arguments
cmd_check()
{
    [ $ARGC -eq 1 ] && [ $HELP -eq 0 ] && show_help
    [ $ARGC -eq 1 ] && [ $LICENSE -eq 0 ] && show_license
    [ $ARGC -eq 1 ] && [ $SCRIPT_VERSION -eq 0 ] && show_version
    [ $ARGC -gt 1 ] && [ $HELP -eq 0 ] && log_fail "$EMSG"
    [ $ARGC -gt 1 ] && [ $LICENSE -eq 0 ] && log_fail "$EMSG"
    [ $ARGC -gt 1 ] && [ $SCRIPT_VERSION -eq 0 ] && log_fail "$EMSG"

    checksum=1
    [ -z "$VERSION" ] || checksum=$((checksum+1))
    [ $YES -eq 0 ] && checksum=$((checksum+1))
    [ $FORCE -eq 0 ] && checksum=$((checksum+1))
    [ $ARGC -eq $checksum ] || log_fail "$EMSG"

    case $DISTRO in
	arch)
	    ;;
	freebsd)
	    ;;
	*) log_fail "$EMSG"
	   ;;
    esac
}


# Shows copyright blurb
show_blurb()
{
    echo 'provision -- Plexus Rosetta provisioning tool.'
    echo 'This utility is part of Plexus Rosetta.'
    echo ''
    echo 'Copyright (c) 2022 Abhishek Chakravarti.'
    echo 'All rights reserved.'
    echo ''
    echo 'This is free and open source software; you are free to redistribute'
    echo 'it under the provisions of the BSD 3-Clause License. There is NO'
    echo 'WARRANTY, to the extent permitted by law. Run provision -L to show'
    echo 'the full license text.'
    echo ''
}


# Shows the usage text
show_help()
{
    show_blurb
    
    printf 'Usage: provision [OPTION...] DISTRO\n\n'
    printf 'Options:\n'
    printf ' -H\tshow usage\n'
    printf ' -L\tshow license details\n'
    printf ' -V\tshow version info\n\n'
    printf ' -f\tforce regeneration of Vagrantfile\n'
    printf " -k\tSSH key (default \$HOME/.ssh/id_rsa)\n"
    printf ' -v\tdistribution version\n'
    printf ' -y\tinstall required binaries\n'
    printf 'Distributions:\n'
    printf ' arch\t\tArch Linux\n'
    printf ' freebsd\tFreeBSD\n\n'
    printf 'FreeBSD Versions:\n'
    printf ' 12.3\tsupported until 31 March 2023\n'
    printf ' 13.1\tsupported until 30 June 2023 (default)\n'

    exit 0
}


# Show version information
show_version()
{
    show_blurb
    
    echo 'Version: 0.1'
    echo 'Commit: fb137f87d021cf4a4eda79e4c13302ae0b2291c7'
    echo 'Date: Wed Sep 21 18:43:52 2022 +0530'

    exit 0
}


# Shows the license text
show_license()
{
    echo 'PLEXUS ROSETTA LICENSE'
    echo ''
    echo 'Copyright (c) 2022 Abhishek Chakravarti.'
    echo 'All rights reserved.'
    echo ''
    echo 'Redistribution and use in source and binary forms, with or without'
    echo 'modification, are permitted provided that the following conditions'
    echo 'are met:'
    echo '1. Redistributions of source code must retain the above copyright'
    echo '   notice, this list of conditions and the following disclaimer.'
    echo '2. Redistributions in binary form must reproduce the above copyright'
    echo '   notice, this list of conditions and the following disclaimer in'
    echo '   the documentation and/or other materials provided with the'
    echo '   distribution.'
    echo '3. Neither the name of the copyright holder nor the names of its'
    echo '   contributors may be used to endorse or promote products derived'
    echo '   from this software without specific prior written permission.'
    echo ''
    echo 'THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS'
    echo '"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT'
    echo 'LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR'
    echo 'A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT'
    echo 'HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,'
    echo 'INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,'
    echo 'BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS'
    echo 'OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED'
    echo 'AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT'
    echo 'LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY'
    echo 'WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE'
    echo 'POSSIBILITY OF SUCH DAMAGE.'

    exit 0
}


#
# VAGRANT FUNCTIONS
#


# Checks whether Vagrant is installed
vg_check()
{
    case "$OS_DISTRO" in
	arch)
	    if [ $YES -eq 0 ]; then
		pkg_install vagrant
	    else
		pkg_check vagrant
	    fi
	    ;;
	*)
	    emsg="provisioning with Vagrant not supported for $OS_DISTRO"
	    log_fail "$emsg"
	    ;;
    esac
}


# Processes checks related to Vagrantfile
vg_check_file()
{
    if [ -f Vagrantfile ]; then
	log_info 'Vagrantfile found'
	
	if [ $FORCE -eq 0 ]; then
	    log_warn '-f option set, removing Vagrantfile'

	    vagrant halt >/dev/null 2>&1
	    vagrant destroy -f >/dev/null 2>&1
	    rm -f Vagrantfile >/dev/null 2>&1

	    log_ok 'Vagrantfile removed'
	    vg_write
	fi

	if vagrant status | grep 'is running' >/dev/null 2>&1; then
	    log_ok 'Vagrant box already running, skipping'
	    exit 0
	fi
    else
	vg_write
    fi
}


# Boots Vagrant box
vg_boot()
{
    log_info 'booting Vagrant box'
    vagrant up || log_fail 'Vagrant box failed to boot'
    log_ok 'Vagrant box ready'
}


# Generates Vagrantfile
vg_write()
{
    case $DISTRO in
	arch)
	    vg_arch
	    ;;
	freebsd)
	    vg_freebsd
	    ;;
	*)
	    log_fail "$EMSG"
	    ;;
    esac

    log_ok 'Vagrantfile generated'
}


# Wrtes Vagrantfile for Arch Linux
vg_arch()
{
    log_info 'generating Vagrantfile for Arch Linux'

    {
	echo "Vagrant.configure(\"2\") do |config|"
	echo '  config.vm.box = "generic/arch"';
        echo '  config.vm.define "plexus_sybil_arch"';
        echo '  config.ssh.forward_agent = true';
        echo '  config.vm.provision "shell", inline: <<-SHELL';
        echo '    pacman -Syyu --noconfirm';
        echo '    pacman -S --needed --noconfirm base-devel glibc git go';
        echo '    pacman -S --noconfirm pacman-contrib';
        echo '    git clone https://aur.archlinux.org/yay.git';
        echo '    chown -R vagrant:vagrant yay';
        echo '    su - vagrant -c "cd yay; makepkg -si --noconfirm"';
        echo '  SHELL';
        echo 'end';
    } > Vagrantfile
}


# Writes Vagrantfile for FreeBSD
vg_freebsd()
{
    log_info 'generating Vagrantfile for FreeBSD'

    [ -z "$VERSION" ] && box='generic/freebsd13'
    case $VERSION in
	12.3)
	    box='generic/freebsd12'
	    ;;
	13.1)
	    box='generic/freebsd13'
	    ;;
	*)
	    if [ -z "$VERSION" ]; then
		box='generic/freebsd13'
	    else
		log_fail "$EMSG"
	    fi
	    ;;
    esac

    {
	echo "Vagrant.configure(\"2\") do |config|"
	echo "  config.vm.box = \"$box\"";
        echo '  config.vm.define "plexus_sybil_freebsd"';
        echo '  config.ssh.forward_agent = true';
        echo '  config.vm.provision "shell", inline: <<-SHELL';
        echo '    pkg update';
        echo '    pkg upgrade -y';
        echo '    pkg install -y git';
        echo '  SHELL';
        echo 'end';
    } > Vagrantfile
}


# Clones Plexus Rosetta Git repository inside Vagrant box
vg_clone()
{
    #repo=plexus-rosetta
    #origin=git@github.com:achakravarti/plexus-rosetta.git

    #log_info "adding SSH key $SSHKEY"
    #eval "$(ssh-agent -s)"
    #ssh-add "$SSHKEY"
    #log_ok 'SSH key added'

    #log_info "testing SSH key $SSHKEY"
    #vagrant ssh -c 'ssh -o StrictHostKeyChecking=no -T git@github.com'
    #[ $? -eq 1 ] || log_fail "test for $SSHKEY failed"
    #log_ok "$SSHKEY tested successfully"
    
    repo=plexus/sybil
    origin=https://codeberg.org/plexus/sybil.git

    log_info "looking for repository $repo"

    if vagrant ssh -c "ls $repo"; then
	log_ok "$repo repository found, skipping"
    else
	vagrant ssh -c "git clone --recurse-submodules $origin" \
	    || log_fail "failed to clone $origin"

	log_ok "cloned $origin, configure and make as required"
    fi
}


#
# MAIN ENTRY POINT
#


# Main entry point of script
main()
{
    cmd_parse "$@"
    cmd_check
    show_blurb
    vg_check
    vg_check_file
    vg_boot
    vg_clone
}


# Run script
main "$@"

